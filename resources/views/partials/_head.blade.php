{{-- This file contains the header which is included in the main layout at the time of compiling --}}
{{-- The header contains the title, common stylesheets & other individual sheets specific to a page that you will add in that page will be yielded here & then included in the main layout file.  --}}

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

{{-- You can & should set different title for different pages. The title for each page will be yielded here & then concatenated with CAD2017 & then displayed.  --}}
<title>CAD2017 @yield('title') </title>

<!-- Bootstrap -->
{{-- <link href="css/bootstrap.css" rel="stylesheet"> --}}
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

{{-- FOnt & Font-awesome stylesheets/linksheets --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Sacramento" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext" rel="stylesheet">

<link href="public/css/style.css" rel="stylesheet">

<link href="public/css/mediaQuery.css" rel="stylesheet">


@yield('stylesheets')