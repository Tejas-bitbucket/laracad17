@extends('layout/main')

@section('title', '| Homepage')

 <div style="background-color: #001F6C">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  aligment_header">
                <h1 style="color: #fff">CAD CONFERENCE</h1>
                <h4 style="color: #fff">Mumbai</h4>
                <h4 style="color: #fff">22<sup>nd</sup> - 24<sup>th</sup> September 2017</h4>
                <h4 style="color: #fff" class="fontcolor">One and Only Conference where Cardiology Meets Diabetes</h4>

                <a href="cad_registrationForm.html"> <button style="margin: 40px auto auto; display: block; padding: 8px 24px; color: rgb(0, 0, 0); font-size: 1.2em; background-color: rgb(255, 255, 255); border: medium none;">Registration Form</button> </a>
                <div class="img_mumbai">
                    <img  src="public/images/background/mumbai(new).png">
                </div>
            </div>
        </div>
    </div>
</div>