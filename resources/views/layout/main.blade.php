{{-- main layout:-All the partials are included & every page extends this layout--}}

<!DOCTYPE html>

<html lang="en">

<head>
	@include('partials._head')
</head>

<body>
	@include('partials._nav')
	
	<div class="container"> {{-- Start of the main container --}}

	@include('partials._messages')

	@yield('content')

	<b>
		<hr>
	</b>

	@include('partials._footer')

	</div> {{-- End of main container --}}

	@include('partials._javascript')

	@yield('scripts')

</body>

</html>